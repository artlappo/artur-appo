Artur �appo

This is an implementation of genetic algorithm, which learns how to write a sentence.

How to compile

I've prepared packaged version of this app already. If you want to compile project on your own you can use:

   IntelliJ IDEA IDE to compile project or,
   Maven to compile and pack an app to jar archive.

How to run this app

    java -cp SimpleStudentsMenager-1.0.0-SNAPSHOT.jar main
