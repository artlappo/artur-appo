
public class CannotFindException extends Exception {
    public CannotFindException() {
    }

    public String getMessage() {
        return "Program could not find proper solution!" + super.getMessage();
    }

    public String toString() {
        return "CannotFindException!\n";
    }
}