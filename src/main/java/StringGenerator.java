import java.util.Random;

public class StringGenerator {

    void generateChromosome(int targetLen, char genes[]){
        Random generator = new Random();
        for(int i = 0; i<targetLen; i++){
            int a = generator.nextInt(127-32)+32;
            genes[i] = (char)a;
        }
    }
}
