import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Population extends DNA implements FitnessCalculatable{

    DNA storage[];
    List<DNA> matingPool;
    int quantity;
    String target;
    int lenght;
    double mutation_rate;
    long generations = 1;
    boolean finished = false;
    DNA best;
    int perfectScore;

    Population(int quantity, String target, double mutation_rate) {
        this.quantity = quantity;
        this.storage = new DNA[this.quantity];
        this.target = target;
        this.lenght = this.target.length();
        this.mutation_rate = mutation_rate;
        this.generations = 1;
        this.finished = false;
        this.perfectScore = this.lenght;
        this.populate();
        this.naturalSelection();
    }

    public void calculateFitness(){
        for (int i = 0; i < this.quantity; i++){
            this.storage[i].calculateFitness(this.target);
        }
    }

    void populate() {
        for (int i = 0; i < this.quantity; i++) {
            storage[i] = new DNA(this.target.length());
        }
        this.calculateFitness();
    }

    void naturalSelection() {
        this.matingPool = new ArrayList<DNA>();
        for (DNA a : this.storage) {
            for (int i = 0; i < a.fitness; i++) {
                this.matingPool.add(a);
            }
        }
    }

    void generate()throws CannotFindException{
        DNA child;
        Random gen = new Random();
        DNA newPop[] = new DNA[this.quantity];
        for(int i = 0; i< this.storage.length; i++){
            try {
                int a = gen.nextInt(this.matingPool.size());
                int b = gen.nextInt(this.matingPool.size());
                DNA partnerA = this.matingPool.get(a);
                DNA partnerB = this.matingPool.get(b);
                child = partnerA.crossOver(partnerB);
                child.mutate(this.mutation_rate);
                newPop[i] = child;
            } catch (Exception e) {
                newPop[i] = this.storage[i];
                newPop[i].mutate(this.mutation_rate);
            }
        }
        this.generations +=1;
        this.storage = newPop;
        if(this.generations > 200000){
            throw new CannotFindException();
        }
    }

    void evaluate(){
        int bestMatch = 0;
        for(DNA el : this.storage){
            if(el.fitness > bestMatch) {
                bestMatch = el.fitness;
                this.best = el;
            }
        }
        if(bestMatch == this.perfectScore){
            this.finished = true;
        }
    }
}

