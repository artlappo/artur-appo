
public interface FitnessCalculatable {
    void calculateFitness(String target);
}
