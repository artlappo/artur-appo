import java.util.Arrays;
import java.util.Random;

public class DNA extends StringGenerator implements FitnessCalculatable{
    int targetLen;
    int fitness;
    char[] genes;


    DNA(int targetLen_a){
        this.targetLen = targetLen_a;
        genes = new char[this.targetLen];
        this.generateChromosome(this.targetLen, this.genes);
    }
    DNA(){
    }
    String getPhrase(){
        String phrase = "";

        for (char letter: this.genes) {
            phrase += letter;
        }
        return phrase;
    }

    public void calculateFitness(String target){
        int score = 0;
        for(int i=0; i<this.genes.length; i++){
            if(this.genes[i]==target.charAt(i)){
                score += 1;
            }
        }
        this.fitness = score;
    }
    DNA crossOver(DNA partnerB){
        Random gen = new Random();
        int midpoint = gen.nextInt(this.targetLen);
        DNA child = new DNA(this.targetLen);
        for(int i=0; i<this.genes.length; i++){
            if(i<midpoint){
                child.genes[i] = this.genes[i];
            }
            else{
                child.genes[i] = partnerB.genes[i];
            }
        }
        return child;
    }

    void mutate(double mutation_rate){
        Random gen = new Random();
        for(int i=0; i<this.genes.length; i++){
            if(gen.nextFloat() < mutation_rate){
                this.genes[i] = (char)(gen.nextInt(127-32)+32);
            }
        }
    }

}

