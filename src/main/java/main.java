public class main extends DNA{
    static public void main(String[] args) {

        Population pop = new Population(100, "OOP III is my jam!", 0.01);
        while (pop.finished == false) {
            pop.naturalSelection();
            try {
                pop.generate();
            } catch (CannotFindException e) {
                //e.printStackTrace();
                System.out.println("Proper solution not fount within " + (pop.generations -1) + " iterations.");
                System.out.println("Best sample created:");
                break;
            }
            pop.calculateFitness();
            pop.evaluate();
            System.out.println(pop.generations + ". " + pop.best.getPhrase());
        }
        System.out.println("Found solution after " + pop.generations + " generations. It's '" + pop.best.getPhrase() + "'.");

    }
}


